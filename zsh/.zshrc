export ZSH="/home/vkomanchy/.oh-my-zsh"

# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="crcandy"

zstyle ':omz:update' mode auto      # update automatically without asking

zstyle ':omz:update' frequency 15

DISABLE_MAGIC_FUNCTIONS="true"

HIST_STAMPS="dd.mm.yyyy"

plugins=(git)

source $ZSH/oh-my-zsh.sh

alias rimraf="sudo rm -rf"

alias bat="batcat"
alias lg="lazygit"

alias vpn-start="wg-quick up ~/.config/wireguard-config/wg.conf"
alias vpn-stop="wg-quick down ~/.config/wireguard-config/wg.conf"

export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm

JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/jre/
export JAVA_HOME
export PATH=$PATH:$JAVA_HOME/bin
export PATH=$PATH:/home/vkomanchy/.cargo/bin

unalias gm

export PATH="$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH"

